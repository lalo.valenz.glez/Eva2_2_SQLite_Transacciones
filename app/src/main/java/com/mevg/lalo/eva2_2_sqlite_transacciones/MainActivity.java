package com.mevg.lalo.eva2_2_sqlite_transacciones;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = openOrCreateDatabase("app",MODE_PRIVATE, null);
        db.execSQL("create table if not exists amigos( " +
                "id integer primary key autoincrement, " +
                "name text, " +
                "phone text )");
        db.beginTransaction();
        try {
            db.execSQL("insert into amigos (name, phone) " +
                    "values ('lalo', '6141234567')");
            db.execSQL("insert  into amigos (name, phone) " +
                    "values ('ana', '6147654321')");
            //int val = 1 / 0;
            db.execSQL("insert into amigos (name, phone) " +
                    "values ('alan', '6141726354')");
            db.execSQL("insert into amigos (name, phone) " +
                    "values ('majo', '6147162534')");
            db.setTransactionSuccessful();
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

    }
}
